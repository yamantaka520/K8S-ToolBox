## GitLab 版本控制整合 CI / CD 工具
#### 這個是在學習過程中，撞牆撞最久的。參考很多非官方的安裝方法，發現官方的方法最好裝。無腦式的快速安裝，幾乎一行指令該裝的一次全部完成。

經驗分享：透過官方發佈的安裝方式，唯一缺點是PersistenVolume會在安裝過程中自動建立好並使用系統的空間去做建立，如果計畫中有安排其他獨立儲存空間，做法上會比較複雜。必須在套件安裝完成後用轉移方式轉移到目標空間，並無法在安裝過程中指定。就算在安裝過程中預先建立了也會導致安裝過程出錯無法繼續。
更新：參考第六點，經過實際操作，可以解決自訂PersistenVolume的問題。

參考網址：<br>
https://docs.gitlab.com/charts/installation/<br>
https://docs.gitlab.com/14.10/runner/install/kubernetes.html<br>
https://artifacthub.io/packages/helm/gitlab/gitlab<br>
https://artifacthub.io/packages/helm/gitlab/gitlab-runner<br>
https://artifacthub.io/packages/helm/bitnami/minio<br>
https://docs.gitlab.com/charts/installation/deployment.html<br>
https://docs.gitlab.com/charts/installation/upgrade.html<br>
https://docs.gitlab.com/charts/installation/secrets#initial-root-password<br>
https://docs.gitlab.com/charts/advanced/persistent-volumes/index.html<br>
https://docs.gitlab.com/charts/installation/storage.html<br>

安裝方法：
------

## 1. 建立 NAMESPACE
```bash
kubectl create namespace devops
```
## 2. 安裝 gitlab
```bash
helm repo add gitlab http://charts.gitlab.io/
helm install gitlab gitlab/gitlab --version 6.0.0
helm install --namespace devops gitlab gitlab/gitlab --version 6.0.0 --set certmanager-issuer.email=xxxxx@example.com --set global.hosts.domain=example.com
```
## 3. 建立初始化密碼
```bash
# 初始化密碼範例（自動產一串亂碼做為預設密碼，這在Windows環境下會出現錯誤）
kubectl create secret generic <name>-gitlab-initial-root-password --from-literal=password=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 32)
# 實際操作
kubectl create secret generic gitlab-gitlab-initial-root-password --from-literal=password=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 32)

# 或是直接設定密碼
kubectl create secret generic gitlab-gitlab-initial-root-password --from-literal=password=password
```
## 4. 取得Gitlab初始化密碼
```bash
# 查詢剛剛設定的密碼（Windows環境下並沒有內建base64的指令，可以直接下載一個回來，後續在secert的設置上會常用到，
# 網址：https://www.di-mgt.com.au/base64-for-windows.html 。下載回來後將 base64.exe放置到C:\windows目錄下就能使用了）
kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo
```
## 5. Ingress 使用域名
```bash
kubectl get ingress -n devops
NAME                        CLASS          HOSTS                  ADDRESS   PORTS     AGE
cm-acme-http-solver-22qds   gitlab-nginx   kas.example.com                  80        3h29m
cm-acme-http-solver-45k4n   gitlab-nginx   minio.example.com                80        3h29m
cm-acme-http-solver-hkn4c   gitlab-nginx   registry.example.com             80        3h29m
cm-acme-http-solver-zj25l   gitlab-nginx   gitlab.example.com               80        3h29m
gitlab-kas                  gitlab-nginx   kas.example.com                  80, 443   3h30m
gitlab-minio                gitlab-nginx   minio.example.com                80, 443   3h30m
gitlab-registry             gitlab-nginx   registry.example.com             80, 443   3h30m
gitlab-webservice-default   gitlab-nginx   gitlab.example.com               80, 443   3h30m
```
## 6. 使用自訂 PersistenVolume
參考網址：<br>
https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/examples/storage/use_manual_volumes.yml<br>
```bash
# 編輯PersistenVolume設定檔
vi use_manual_volumes.yaml
# 以自訂設定檔安裝
helm install --namespace devops gitlab gitlab/gitlab --version 6.0.0 -f use_manual_volumes.yaml --set certmanager-issuer.email=xxxxx@example.com --set global.hosts.domain=example.com --set global.hosts.externalIP=10.10.10.10
```