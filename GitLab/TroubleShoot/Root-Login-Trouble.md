## 安裝完成後卻發現所有的登入方式都無法登入！

## 使用RADME.md 中的方法，安裝完成後還是無法登入。
網路上查到的預設帳號：root，預設密碼：5iveL!fe。但是.....不是那麼簡單的！還是不是這個！<br>
查到 Reset Password的指令，參考網址：https://docs.gitlab.com/ee/security/reset_user_password.html<br>
```bash
sudo gitlab-rake "gitlab:password:reset[root]"
```
但是在K8S中，該到哪邊去執行這個指令呢？我們查詢一下所有在執行中的Pod看看哪個比較可疑！<br>
(這邊依照我們的布建範例：使用的namespace為devops)<br>
```bash
kubectl get pod -n devops

# return
NAME                                                   READY   STATUS      RESTARTS         AGE
gitlab-certmanager-6cff5657f7-sfp49                    1/1     Running     1 (14m ago)      86m
gitlab-certmanager-cainjector-5b5f7f56bb-xxdws         1/1     Running     1 (14m ago)      86m
gitlab-certmanager-webhook-9b66bbc57-6xgzg             1/1     Running     1 (14m ago)      86m
gitlab-gitaly-0                                        1/1     Running     1 (14m ago)      86m
gitlab-gitlab-exporter-698d79fddc-d8kj2                1/1     Running     1 (14m ago)      86m
gitlab-gitlab-runner-59fdd788c8-rgx2n                  0/1     Running     10 (2m21s ago)   86m
gitlab-gitlab-shell-c4bdddb69-2b9v5                    1/1     Running     1 (14m ago)      86m
gitlab-gitlab-shell-c4bdddb69-n8npm                    1/1     Running     1 (14m ago)      85m
gitlab-issuer-1-jnzws                                  0/1     Completed   0                86m
gitlab-kas-6cdc969f4b-r7f7k                            1/1     Running     1 (14m ago)      85m
gitlab-kas-6cdc969f4b-vvqhc                            1/1     Running     1 (14m ago)      86m
gitlab-migrations-1-z8292                              0/1     Completed   1                86m
gitlab-minio-74dfc6b6c7-h9f7p                          1/1     Running     1 (14m ago)      86m
gitlab-minio-create-buckets-1-t4c85                    0/1     Completed   0                86m
gitlab-nginx-ingress-controller-5f5966bd86-5k9v9       1/1     Running     1 (14m ago)      86m
gitlab-nginx-ingress-controller-5f5966bd86-c6f9z       1/1     Running     1 (14m ago)      86m
gitlab-nginx-ingress-defaultbackend-576f497646-p5zk2   1/1     Running     1 (14m ago)      86m
gitlab-postgresql-0                                    2/2     Running     2 (14m ago)      86m
gitlab-prometheus-server-6444574bcb-8mzr6              2/2     Running     2 (14m ago)      86m
gitlab-redis-master-0                                  2/2     Running     2 (14m ago)      86m
gitlab-registry-665c6c7c-6kxjf                         1/1     Running     1 (14m ago)      86m
gitlab-registry-665c6c7c-zjbmn                         1/1     Running     1 (14m ago)      85m
gitlab-sidekiq-all-in-1-v2-58b84786db-z6zps            1/1     Running     1 (14m ago)      86m
gitlab-toolbox-754f585c45-n6cxw                        1/1     Running     1 (14m ago)      86m
gitlab-webservice-default-77476cf95d-7h9ls             2/2     Running     2 (14m ago)      86m
gitlab-webservice-default-77476cf95d-rgbqw             2/2     Running     2 (14m ago)      85m
nexus-86d7b9f6f8-4qtvx                                 1/1     Running     1 (14m ago)      141m
```
這邊看起來應該是 gitlab-toolbox 這個Pod最有機會。來試試～
```bash
kubectl exec -it gitlab-toolbox-754f585c45-n6cxw bash -n devops
# return
git@gitlab-toolbox-754f585c45-n6cxw:/$
# 接著輸入
git@gitlab-toolbox-754f585c45-n6cxw:/$ gitlab-rake "gitlab:password:reset[root]"
# return
Enter password:
Confirm password:
Password successfully updated for user with username root.
```
看起來應該是了！嘗試登入一下，果然可以了！