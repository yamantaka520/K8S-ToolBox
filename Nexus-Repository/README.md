## Nexus Repository 是一個相當好用的 Image Repository 管理軟體
#### Nexus Repository 常用在自建 Docker Image Repository 管理，尤其當考量應用環境無法使用公開的Docker Hub之類的使用空間時。自建一個就會用到。
參考網址：<br>
https://developer.entando.com/v6.2/tutorials/ecr/how-to-setup-nexus-on-kubernetes-cluster.html#requirements<br>

安裝步驟：
## 1. Create a namespace for nexus
```bash
kubectl create namespace devops
```
## 2. 建立 Nexus PersistentVolume 持續儲存區
```bash
kubectl apply -f nexus-pv.yaml
kubectl get persistentvolumes
kubectl apply -f nexus-pvc.yaml
kubectl get pvc
```
## 3. 開始安裝部署 Nexus Pepository
```bash
kubectl create -f nexus-deployment.yaml
kubectl describe pod nexus -n devops
```
## 4. 建立 nexus service
```bash
kubectl create -f nexus-service.yaml
kubectl describe service nexus-service -n devops
```
## 5. 發布 Nexus Ingress
```bash
kubectl create -f nexus-ingress.yaml
```
> 管理網頁存取<br>
>> http://ExternalIP:8081<br>
>> 預設管理者帳號：admin<br>
>> 取得管理者密碼<br>
>> ```bash
>> # get the password from within the container with cat /nexus-data/admin.password
>> kubectl exec pod_name -- bash -c "cat /sonatype-work/admin.password"
>> ```