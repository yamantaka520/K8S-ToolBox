# MatelLB K8S LoadBalancer Plug-in 套件
## 個人覺得在自架K8S應用上相當好用的工具，可以透過Loadbalancer導通不需要透過Node Port或是走Port forwer。可直接定義ExternalIP 可以是一段IP，也可以定義成網段。<br>
## 這邊選用 bitnami 這個發布者的套件，這個發布者整合了相當多的套件可以參考使用。<br>
參考網址：<br>
https://metallb.universe.tf/installation/<br>
https://ithelp.ithome.com.tw/articles/10221722<br>
https://bitnami.com/stack/metallb/helm<br>

安裝經驗分享：為何沒使用官方的安裝套件？不知為何，在我測試的環境中依照官方的安裝步驟操作後，Pod一直Crash掉，起不來。最後使用bitnami發布的套件，很簡單就完成了。

安裝 MetalLB 套件
```bash
# 建立 NameSpace
kubectl apply -f metallb-namespace.yaml
# 添加 MetalLB Repository
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm repo list
# 預設值安裝
helm install metallb bitnami/metallb --namespace=metallb-system
# 自訂設定值安裝
helm install metallb bitnami/metallb -f metallb-values.yaml --namespace=metallb-system
#  安裝後套用網路設定
kubectl apply -f metallb-values.yaml
```
編輯 MetalLB 網路設定
```bash
vi metallb-values.yaml
```

設定IP固定與同一IP共用端口
```
apiVersion: v1
kind: Service
metadata:
  name: dns-service-tcp
  namespace: default
  annotations:
    metallb.universe.tf/allow-shared-ip: "key-to-share-1.2.3.4"
spec:
  type: LoadBalancer
  loadBalancerIP: 1.2.3.4
  ports:
    - name: dnstcp
      protocol: TCP
      port: 53
      targetPort: 53
  selector:
    app: dns
---
apiVersion: v1
kind: Service
metadata:
  name: dns-service-udp
  namespace: default
  annotations:
    metallb.universe.tf/allow-shared-ip: "key-to-share-1.2.3.4"
spec:
  type: LoadBalancer
  loadBalancerIP: 1.2.3.4
  ports:
    - name: dnsudp
      protocol: UDP
      port: 53
      targetPort: 53
  selector:
    app: dns
```
一般應用範例：
```
apiVersion: v1
kind: Service
metadata:
  name: nginx
  namespace: default
  annotations:
      prometheus.io/scrape: 'true'
      prometheus.io/path:   /
      prometheus.io/port:   '8081'
      metallb.universe.tf/address-pool: LB-IP-Pool
      # 或是
      metallb.universe.tf/allow-shared-ip: "key-to-share-192.168.100.163"
spec:
  selector:
    app: nginx
  type: LoadBalancer
  # 這兩個用法都可用
  externalIPs:
    - 192.168.100.163
  # 或是  
  loadBalancerIP: 192.168.100.163
  ports:
    - name: nginx-lb-port 
      port: 8081
      targetPort: 8081
```