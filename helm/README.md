## Helm K8S 套件管理工具
#### Helm是一套K8S用的套件管理程式，可以透過這個管理程式安裝一些整合好的工具也可以將自己整合好的套件發布。相當方便與簡單使用。但是也不是完全無雷就是了。
參考網址：<br>
https://helm.sh/docs/intro/install/<br>

安裝步驟（Ubuntu 20.04）
```bash
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
```