#### 使用非公開的 Docker Hub 資源下載私有 Image
## 在非公開的 Docker Repository中要怎麼導入K8S中使用？

## 1. 建立 Secret資料
```bash
# 範例：(從這邊我們就能發現，如果是自己架設Nexus Repository，就能依照這個方式取得Image了。)
kubectl create secret docker-registry <name> \
--docker-server=<your-registry-server> \
--docker-username=<your-name> \
--docker-password=<your-pword> \
--docker-email=<your-email>

# 實作：
kubectl create secret docker-registry my-docker-private-hub \
--docker-server=https://index.docker.io/v1/ \
--docker-username=myusername \
--docker-password=mypassword \
--docker-email=home@home.net

# return
secret "my-docker-private-hub" created
```

## 2. 在 Docker Hub上建立一個 Private Repository(myname/myprivateimage)

## 3. 建立 Pod 要執行的 YAML
```yml
# My Private App Pod YAML file : myprivatepod.yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: my-private-app
spec:
  containers:
  - name: my-private-app
    image: myname/myprivateimage   <=== 指定使用私有映像檔（如果是自架Nexus Repository 這邊用法就會是 nexusIP:ServicePort/myprivateimage）
    imagePullPolicy: Always
  imagePullSecrets:   <=== 下載映像檔時套用先前建立的 Secret 物件
  - name: my-docker-private-hub
```

## 4. 部署到 K8S
```bash
kubectl apply -f myprivatepod.yaml
# return
pod "my-private-app" created

# 查詢執行狀態
kubectl get pod my-private-app
# return
NAME           READY     STATUS    RESTARTS   AGE
my-private-app    1/1       Running   0          18s

# 部署詳細資料查詢
kubectl describe pod my-private-app
```