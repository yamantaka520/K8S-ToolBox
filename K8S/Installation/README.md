#### 此安裝步驟是使用Ubuntu 20.04.4 TLS作為主要作業系統進行安裝。安裝最低需求建議為3個節點，一個作為Master Node，兩個作為Work Node作為基礎的執行環境。
#### 這個環境當中的Container Runtime是使用Docker作為執行環境。網路插件是使用Calico。
#### 安裝設定是以Default作為基礎，修改IPVS模式會另外增加說明。

###### 安裝步驟
------

## 1. 修改主機設定與進行更新至最後Patch。
```bash
# 更新
sudo apt-get update
sudo apt upgrate -y

# 設定時區
sudo timedatectl set-timezone Asia/Taipei
sudo timedatectl

# 關閉SWAP
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
sudo cat /etc/fstab

# 修改主機名稱(例如：MasterNode: K8S-Master-Node，WorkNode: K8S-Node-1, K8S-Node-2，也可以不用修改，後續說明會依此作為主機名稱說明)
sudo vi /etc/hosts
sudo vi /etc/hostname

# 重新開機
sudo reboot
```
## 2. 安裝Container Runtime(所有主機都要)
參考網址：<br>
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04<br>
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04<br>

```bash
# 安裝 Docker 執行環境
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt-cache policy docker-ce
sudo apt install docker-ce containerd.io docker-ce-cli
sudo systemctl status docker

# 修改 Docker 執行權限給使用者
sudo usermod -aG docker ${USER}
su - ${USER}
groups

# 安裝 docker-compose 執行環境（K8S上其實用不到，但是方便測試用習慣上會放上去）
sudo curl -L "https://github.com/docker/compose/releases/download/v2.6.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker --version
docker-compose --version
```

## 3. K8S 安裝
參考網址：<br>
https://computingforgeeks.com/deploy-kubernetes-cluster-on-ubuntu-with-kubeadm/<br>
https://www.digitalocean.com/community/tutorials/how-to-create-a-kubernetes-cluster-using-kubeadm-on-ubuntu-20-04<br>

3.1. 所有節點都需要執行的環境設置：<br>
```bash
# 安裝必要套件與新增 K8S 的 APT Repository
sudo apt -y install curl apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" >> /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update

# 修改 Kernel Module
sudo modprobe overlay
sudo modprobe br_netfilter
sudo lsmod | grep br_netfilter
sudo lsmod | grep overlay

# 修改系統設定
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
  net.bridge.bridge-nf-call-ip6tables = 1
  net.bridge.bridge-nf-call-iptables = 1
  net.ipv4.ip_forward = 1
  EOF
sudo mkdir /etc/docker
cat <<EOF | sudo tee /etc/docker/daemon.json
  { "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts":
  { "max-size": "100m" },
  "storage-driver": "overlay2"
  }
  EOF

# 設定防火牆
sudo ufw allow 6443
sudo ufw allow 6443/tcp

# 套用系統設定
sudo sysctl --system
sudo systemctl enable docker
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo reboot
```

3.2. 安裝K8S套件
```bash
# 在Master Node:
sudo apt-get install kubelet kubeadm kubectl kubernetes-cni -y
kubectl version --client && kubeadm version

# 在Work Node:
sudo apt-get install kubelet kubeadm kubernetes-cni -y
kubectl version --client && kubeadm version

# 在 Master Node 初始化 K8S（建立K8S Cluster內部網路，可以自訂一個區段的Private IP，通常習慣上會使用一個Class B，這邊我們使用 10.224.0.0/16）
sudo kubeadm init --pod-network-cidr=10.244.0.0/16

# -- or -- 如果單純執行上面那行初始化有問題，可以改用下面這行執行
sudo kubeadm init --ignore-preflight-errors=NumCPU,Mem --pod-network-cidr=10.244.0.0/16
```

3.3. 初始化完成後會出現以下畫面（這個很重要，最好存擋下來。）

```bash
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:
# 這個步驟可以把管理設定檔複製給其他使用者，具備不用使用root帳號也能夠對K8S進行管理
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:
# 為root帳號設定K8S管理設定檔
  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:
# 這行很重要一定要存下，後面要加入Work Node都必須用到
kubeadm join 10.2.32.1:6443 --token xxxxxxx.xxxxxxxxxxxxxxxx \
        --discovery-token-ca-cert-hash sha256:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

# 查詢 K8S Cluster 狀態
kubectl cluster-info
```

3.4. 安裝K8S網路模型與網路插件（在 Master Node 執行）
```bash
# 基礎網路模型（Flannel）
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/k8s-manifests/kube-flannel-rbac.yml

# 網路插件（Calico）
kubectl create -f https://docs.projectcalico.org/manifests/tigera-operator.yaml 
kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml

# 查看元件狀態
kubectl get componentstatus
kubectl get cs
```

3.5. 加入 Work Node
```bash
# 在每台 Work Node 執行
kubeadm join 10.2.32.1:6443 --token xxxxxxx.xxxxxxxxxxxxxxxx \
        --discovery-token-ca-cert-hash sha256:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

# 檢查 K8S Cluster 內基礎服務是否都已啟動
watch kubectl get pods --all-namespaces

# 查看 K8S Cluster 內各 Node 狀態
kubectl get nodes -o wide
```
