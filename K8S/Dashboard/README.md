#### 安裝 Kubernetes DashBoard
## 1. 安裝
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.6.0/aio/deploy/recommended.yaml
```
## 2. 修改 Service Type
```bash
kubectl edit svc kubernetes-dashboard -n kubernetes-dashboard

# 編輯 Service Type 為 LoadBalancer 或是 NodePort 
type: ClusterIP --> LoadBalancer
# 新增 LoadBalancerIP (如果使用的是LoadBalancer)
loadBalancerIP: xxx.xxx.xxx.xxx
```
## 3. 建立 Service Account
```bash
# 編輯建立帳號 k8s-admin.yaml檔
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
# 建立帳號
kubectl apply -f k8s-admin.yaml
# 編輯權限設置 k8s-admin-role.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
# 套用權限
kubectl apply -f k8s-admin-role.yaml
```
## 4. 建立帳號 Token
```bash
kubectl -n kubernetes-dashboard create token admin-user
## 這邊會得到 token，用於登入 DashBoard
```