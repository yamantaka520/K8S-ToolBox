## VolumeSnapshotter K8S PersistenVolume Backup & Restore 
#### K8S的官方功能，但是已經移出標準API範圍內，所以必須要手動安裝進去。
參考網址：<br>
https://kubernetes.io/blog/2020/12/10/kubernetes-1.20-volume-snapshot-moves-to-ga/<br>
https://github.com/kubernetes-csi/external-snapshotter/tree/master/client/config/crd<br>
https://kubernetes.io/docs/concepts/storage/volume-snapshots/<br>

安裝方式：
-------

```bash
# 直接到Github上，將檔案下下來，進到目錄後直接執行下面指令。
kubectl apply -k ./
```