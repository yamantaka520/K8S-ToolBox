
## ELK K8S Log 管理，查詢與分析工具
#### 強大的Log管理分析工具整合，由 Elasticsearch, Kibana, Logstash, Metricbeat, Filebeat 幾個元件所組成。
參考網址<br>
https://ithelp.ithome.com.tw/m/articles/10256470?sc=rss.qu<br>
https://itnext.io/deploy-elastic-stack-on-kubernetes-1-15-using-helm-v3-9105653c7c8<br>

安裝方式：
------

## 1. 新增Helm repository
```bash
helm repo add elastic https://helm.elastic.co
helm repo update
```
## 2. 建立ELK namespace
```bash
kubectl create namespace elk-system
```
## 3. 建立ELK 儲存區
```bash
kubectl apply -f elasticsearch-pv.yaml
```
## 4. 安裝 ElasticSearch
```bash
helm install elasticsearch elastic/elasticsearch --namespace elk-system -f elasticsearch-values.yaml --version 7.17.3
```
## 5. 安裝 Kibana
```bash
helm install kibana elastic/kibana --namespace elk-system -f kibana-values.yaml --version 7.17.3
```
> 連線測試：
> http://LoadBalance_IP:5601/app/infra#/infrastructure/inventory

## 6. 安裝 Metricbeat
```bash
helm install metricbeat elastic/metricbeat --namespace elk-system -f metricbeat-values.yaml
```
## 7. 安裝 Filebeat
```bash
helm install metricbeat elastic/metricbeat --namespace elk-system --version 7.17.3
```
## 8. 安裝 Logstash
```bash
helm install logstash elastic/logstash --namespace elk-system -f logstash-values.yaml --version 7.17.3
```