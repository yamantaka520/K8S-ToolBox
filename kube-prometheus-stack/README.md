## kube-prometheus-stack K8S 監控管理
#### Prometheus 與 Grafana 整合套件，一次把需要的安裝完成。
參考網址：<br>
https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack<br>
https://vocus.cc/article/61e60ba9fd897800017098d9<br>
https://blog.amis.com/kubernetes-operators-prometheus-3584edd72275<br>

安裝方法
-------

## 1. 建立 NameSpace
```bash
kubectl create namespace monitoring
```
檢查建立狀態
```bash
kubectl create namespace monitoring
```
## 2. 建立 PersistentVolume

```bash
kubectl apply -f prometheus-grafana-pv.yaml
```
## 3. 檢查 PersistentVolume 與 PersistentVolumeCliam 建立狀態
```bash
kubectl get pv -n monitoring
kubectl get pvc -n monitoring
```
## 4. 使用 Ｈelm 套件管理程式安裝
```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```
## 5. 預設組態安裝
```bash
helm install kube-prometheus-stack prometheus-community/kube-prometheus-stack --namespace monitoring
```
## 6. 自訂組態安裝
自訂組態安裝需要先行修改組態設定檔
```bash
vi kube-prometheus-stack.yaml
```
自訂組態安裝
自訂組態安裝需要先行修改組態設定檔
```bash
helm install kube-prometheus-stack prometheus-community/kube-prometheus-stack --namespace monitoring --values kube-prometheus-stack.yaml
```
## 7. 修改 K8S Service 狀態
預設狀態下 Service Type 是使用 ClusterIP，這樣的情況下是無法透過外部網路存取服務的。這個範例中我將運作中的狀態導出後，修改 Service Type 為 LoadBalancer。若是在公有雲服務所提供的K8S環境中，這樣的修改並無問題。但是如果是在自建的K8S環境中則缺乏了這個型態可以選用，因此我這邊使用了MetalLB這個Plug-in去達成這個目的。因此在設定中多了一些設置：
```
apiVersion: v1
kind: Service
metadata:
  annotations:
    meta.helm.sh/release-name: kube-prometheus-stack
    meta.helm.sh/release-namespace: monitoring
    metallb.universe.tf/address-pool: LB-IP-Pool
```
套用 Service 設置
```bash
kubectl apply -f kube-prometheus-stack-alertmanager-service.yaml
kubectl apply -f kube-prometheus-stack-grafana-service.yaml
kubectl apply -f kube-prometheus-stack-prometheus-service.yaml
```
## 8. 使用 Ingress 提供對外服務
編輯YAML檔案中關於域名的相關設定
```bash
vi prometheus-grafana-ingress.yaml
```
套用 Ingress 服務
```bash
kubectl apply -f prometheus-grafana-ingress.yaml
```
Server 套用完成後，其實就能直接透過Services拿到的ExternalIP訪問到Grafana Dashboard。當然也可以在套用完Ingress設置後透過Ingress的域名或IP進行這個訪問了。
```
http://ExternalIP
```
*** Grafana DashBoard 預設帳號：admin 密碼：prom-operator