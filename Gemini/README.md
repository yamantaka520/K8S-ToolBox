## Gemini: Automate Backups of PersistentVolumes in Kubernetes
#### 相當簡易操作的備份回存套件
參考網址：<br>
https://www.fairwinds.com/blog/gemini-automate-backups-of-persistentvolumes-in-kubernetes<br>
https://artifacthub.io/packages/helm/fairwinds-stable/gemini<br>

安裝方法：
-------

```bash
kubectl create namespace gemini
helm repo add fairwinds-stable https://charts.fairwinds.com/stable
helm install gemini fairwinds-stable/gemini --namespace gemini
```
備份方法：
------

```bash
apiVersion: gemini.fairwinds.com/v1beta1
kind: SnapshotGroup
metadata:
  name: postgres-backups
spec:
  persistentVolumeClaim:
    claimName: postgres-data
  schedule:
  - every: 10 minutes
    keep: 3
```
範例：
```bash
apiVersion: gemini.fairwinds.com/v1beta1
kind: SnapshotGroup
metadata:
  name: postgres-backups
spec:
  persistentVolumeClaim:
    claimName: postgres-data
  schedule:
    - every: 10 minutes
      keep: 3
    - every: hour
      keep: 1
    - every: day
      keep: 1
    - every: week
      keep: 1
    - every: month
      keep: 1
    - every: year
      keep: 1
```

回存方法：
------

```bash
# 查詢備份資料
kubectl get volumesnapshot
NAME                           AGE
postgres-backups-1585945609    15m
# 停止Pod服務
kubectl scale all --all --replicas=0
# 執行回存
kubectl annotate snapshotgroup/postgres-backups --overwrite "gemini.fairwinds.com/restore=1585945609"
# 恢復Pod服務
kubectl scale all --all --replicas=1
```

使用新的PersistemVloumeCliam回存：
```bash
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: restore-pvc
spec:
  dataSource:
    name: postgres-backups-1585945609
    kind: VolumeSnapshot
    apiGroup: snapshot.storage.k8s.io
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```