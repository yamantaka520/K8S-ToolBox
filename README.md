# K8S 學習過程中的好用工具收集與學習記錄
收集好用的工具可以方便解決應用上的問題，順便紀錄下過程中的筆記加以歸類

## [K8S 安裝](https://gitlab.com/yamantaka520/K8S-ToolBox/-/tree/main/K8S/Installation)
#### 此安裝步驟是使用Ubuntu 20.04.4 TLS作為主要作業系統進行安裝。安裝最低需求建議為3個節點，一個作為Master Node，兩個作為Work Node作為基礎的執行環境。

## [K8S 操作記錄](https://gitlab.com/yamantaka520/K8S-ToolBox/-/tree/main/K8S/Operation)
#### K8S運維操作的筆記記錄

## [K8S 除錯紀錄](https://gitlab.com/yamantaka520/K8S-ToolBox/-/tree/main/K8S/Troubleshooting)
#### 將實際遇上的問題與除錯歷程跟解決方法的紀錄

## [Helm](https://gitlab.com/yamantaka520/K8S-ToolBox/-/tree/main/helm) K8S 套件管理工具
#### Helm是一套K8S用的套剪管理程式，可以透過這個管理程式安裝一些整合好的工具也可以將自己整合好的套件發布。相當方便與簡單使用。但是也不是完全無雷就是了。
參考網址：<br>
https://helm.sh/docs/intro/install/<br>

## [kube-prometheus-stack](https://gitlab.com/yamantaka520/K8S-ToolBox/-/tree/main/kube-prometheus-stack) K8S 監控管理
#### Prometheus 與 Grafana 整合套件，一次把需要的安裝完成。
參考網址：<br>
https://blog.amis.com/kubernetes-operators-prometheus-3584edd72275<br>
https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack<br>
https://vocus.cc/article/61e60ba9fd897800017098d9<br>

## [MatelLB LoadBalancer](https://gitlab.com/yamantaka520/K8S-ToolBox/-/tree/main/metallb) K8S LoadBalancer Plug-in 套件
#### 個人覺得在自架K8S應用上相當好用的工具，可以透過Loadbalancer導通不需要透過Node Port或是走Port forwer。可直接定義ExternalIP 可以是一段IP，也可以定義成網段。<br>
參考網址：<br>
https://metallb.universe.tf/installation/<br>
https://ithelp.ithome.com.tw/articles/10221722<br>
https://bitnami.com/stack/metallb/helm<br>

## [Nexus Repository](https://gitlab.com/yamantaka520/K8S-ToolBox/-/tree/main/Nexus-Repository) 是一個相當好用的 Image Repository 管理軟體
#### Nexus Repository 常用在自建 Docker Image Repository 管理，尤其當考量應用環境無法使用公開的Docker Hub之類的使用空間時。自建一個就會用到。
參考網址：<br>
https://developer.entando.com/v6.2/tutorials/ecr/how-to-setup-nexus-on-kubernetes-cluster.html#requirements<br>

## [ELK](https://gitlab.com/yamantaka520/K8S-ToolBox/-/tree/main/ELK) K8S Log 管理，查詢與分析工具
#### 強大的Log管理分析工具整合，由 Elasticsearch, Kibana, Logstash, Metricbeat, Filebeat 幾個元件所組成。
參考網址<br>
https://ithelp.ithome.com.tw/m/articles/10256470?sc=rss.qu<br>
https://itnext.io/deploy-elastic-stack-on-kubernetes-1-15-using-helm-v3-9105653c7c8<br>

## [GitLab](https://gitlab.com/yamantaka520/K8S-ToolBox/-/tree/main/GitLab) 版本控制整合 CI / CD 工具
#### 這個是在學習過程中，撞牆撞最久的。參考很多非官方的安裝方法，發現官方的方法最好裝。無腦式的快速安裝，幾乎一行指令該裝的一次全部完成。
參考網址：<br>
https://docs.gitlab.com/charts/installation/<br>
https://docs.gitlab.com/14.10/runner/install/kubernetes.html<br>
https://artifacthub.io/packages/helm/gitlab/gitlab<br>
https://artifacthub.io/packages/helm/gitlab/gitlab-runner<br>
https://artifacthub.io/packages/helm/bitnami/minio<br>
https://docs.gitlab.com/charts/installation/deployment.html<br>
https://docs.gitlab.com/charts/installation/upgrade.html<br>
https://docs.gitlab.com/charts/installation/secrets#initial-root-password<br>
https://docs.gitlab.com/charts/advanced/persistent-volumes/index.html<br>
https://docs.gitlab.com/charts/installation/storage.html<br>

## [VolumeSnapshotter](https://gitlab.com/yamantaka520/K8S-ToolBox/-/tree/main/K8S/VolumeSnapshot) K8S PersistenVolume Backup & Restore 
#### K8S的官方功能，但是已經移出標準API範圍內，所以必須要手動安裝進去。
參考網址：<br>
https://kubernetes.io/blog/2020/12/10/kubernetes-1.20-volume-snapshot-moves-to-ga/<br>
https://github.com/kubernetes-csi/external-snapshotter/tree/master/client/config/crd<br>
https://kubernetes.io/docs/concepts/storage/volume-snapshots/<br>

## [Gemini](https://gitlab.com/yamantaka520/K8S-ToolBox/-/tree/main/Gemini): Automate Backups of PersistentVolumes in Kubernetes
#### 相當簡易操作的備份回存套件，建構在VolumeSnapsoht的功能下。
參考網址：<br>
https://www.fairwinds.com/blog/gemini-automate-backups-of-persistentvolumes-in-kubernetes<br>
https://artifacthub.io/packages/helm/fairwinds-stable/gemini<br>
